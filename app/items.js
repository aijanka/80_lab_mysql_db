const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM item_report', function (error, results) {
            if (error) throw error;
            res.send(results);
        })
    });

    router.post('/', upload.single('image'), (req, res) => {
        const item = req.body;

        if (req.file) {
            item.image = req.file.filename;
        } else {
            item.image = null;
        }

        db.query(
            'INSERT INTO item_report (name, description, image, category_id, location_id) ' + 'VALUES (?, ?, ?, ?, ?)',
            [item.name, item.description, item.image, item.category_id, item.location_id],
            (error, results) => {
                if (error) throw error;

                item.id = results.insertId;
                res.send(item);
            }
        );
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`SELECT * FROM items.item_report WHERE id=${id}`, (error, results) => {
            if (error) throw error;
            res.send(results);
        })
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`DELETE FROM items.item_report WHERE id=${id}`, (error, results) => {
            if (error) throw error;
        })
    });

    router.put('/:id', (req, res) => {
        const id = req.params.id;
        const item = req.body;
        db.query(`UPDATE items.item_report
SET name = ${item.name}, description = ${item.description}, category_id = ${item.category_id}, location_id = ${item.location_id} WHERE id=${id};`);
        res.send(item);
    });

    return router;
};

module.exports = createRouter;