const express = require('express');
const multer = require('multer');
const path = require('path');

const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM categories', function (error, results) {
            if (error) throw error;
            res.send(results);
        })
    });

    router.post('/', (req, res) => {
        const category = req.body;
        console.log(category);
        db.query(
            'INSERT INTO categories (name, description)' +
            'VALUES (?, ?)', [category.name, category.description],
            (error, results) => {
                if (error) throw error;
                category.id = results.insertId;
                res.send(category);
            });
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`SELECT * FROM categories WHERE id=${id}`, (error, results) => {
            if (error) throw error;
            res.send(results[0]);
        })
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`DELETE FROM categories WHERE id=${id}`, (error, results) => {
            if (error) throw error;
        });
    });

    router.put('/:id', (req, res) => {
        const id = req.params.id;
        const item = req.body;
        db.query(`UPDATE categories SET name = ${item.name}, description = ${item.description} WHERE id=${id};`);
        res.send(item);
    });

    return router;
};

module.exports = createRouter;