const express = require('express');

const router = express.Router();

const createRouter = db => {
    router.get('/', (req, res) => {
        db.query('SELECT * FROM locations', function (error, results) {
            if (error) throw error;
            res.send(results);
        })
    });

    router.post('/', (req, res) => {
        const location = req.body;
        db.query(
            'INSERT INTO locations (name, description) ' +
            'VALUES (?, ?) ', [location.name, location.description],
            (error, results) => {
                if (error) throw error;
                location.id = results.insertId;
                res.send(location);
            });
    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`SELECT * FROM locations WHERE id=${id}`, (error, results) => {
            if (error) throw error;
            res.send(results[0]);
        })
    });

    router.delete('/:id', (req, res) => {
        const id = req.params.id;
        db.query(`DELETE FROM locations WHERE id=${id}`, (error, results) => {
            if (error) throw error;
        });
    });

    router.put('/:id', (req, res) => {
        const id = req.params.id;
        const item = req.body;
        db.query(`UPDATE locations SET name = ${item.name}, description = ${item.description} WHERE id=${id};`);
        res.send(item);
    });

    return router;
};

module.exports = createRouter;