const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const items = require('./app/items');
const locations = require('./app/locations');
const categories = require('./app/categories');
const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host: 'localhost',
    user : 'root',
    password: '0777512992Aa',
    database: 'items'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/items', items(connection));
    app.use('/locations', locations(connection));
    app.use('/categories', categories(connection));

    app.listen(port);
})